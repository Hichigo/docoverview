echo ========== BUILDING WIN64 ==========

set ueVersion=4.27
set projectName=DocOverview
set pluginName=DocumentationProject
set pathToPlugin="D:\Projects\UE4\%projectName%\Plugins\%pluginName%\%pluginName%.uplugin"
set buildPath="D:\Projects\UE4\%projectName%\Build\Win64\%pluginName%"
set pathToRunUAT="D:\Programm\UnrealEngine\UE_%ueVersion%\Engine\Build\BatchFiles\RunUAT.bat"

call %pathToRunUAT% BuildPlugin -plugin=%pathToPlugin% -package=%buildPath% -TargetPlatforms=Win64 -VS2019

pause